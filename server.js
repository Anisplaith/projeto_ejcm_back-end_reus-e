const express = require('express');
require('./src/config/dotenv')();
//require('./src/config/sequelize'); para que serve?? /!\

const app = express();
const port = process.env.PORT; // variável PORT  env
//const cors = require('cors');  para que ser??? /!\
const routes = require("./src/routes/routes");


//app.use(express.json()); para que ser??? /!\
app.use(express.urlencoded({extended:true}));
app.use(routes);


app.get('/', (req, res) => {
  res.send('Oi!')
});

app.listen(port, () => {
  console.log(`${process.env.APP_NAME} está rondando em http://localhost:${port}`);
});
    

