const {response} = require("express");
const User = require ("../../models/User");

const create = async (req, res) => { 

    try{
        const user = await User.create(req.body);
        return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});

    } catch(err){
        res.status(500).json({message: "Erro ao cadastrar usuário. Verifique e tente novamente..."});

    }
};

const show = async(req, res) => {
    const {id} = req.params;

    try{
        const user = await User.findByPk(id);
        
            // if para que user caia em um erro caso não exista e apareça uma mensagem para o usuário
        if(user == null){
            user = 'undefined'; 
        }
        return res.status(200).json({user});

    } catch(err){

        return res.status(500).json({message: "O usuário não existe. Verifique o id e tente novamente..."});

    }


};


const index = async(req, res) => {

    try{
        const user = await User.findAll();
        return res.status(200).json({user});


    } catch(err){
        return res.status(500).json({message: "Erro ao encontrar os usuários. Tente novamente..."});
    }

};


const update = async (req, res) => {
    const {id} = req.params;

    try{

        const {updated} = await User.update(req.body, {where: {id: id}});

        if(!updated) {
            const user = await User.findByPk(id);

            // if para que user caia em um erro caso não exista e apareça uma mensagem para o usuário
            if(user == null){
                user = 'undefined'; 
            }
            return res.status(200).json({message: "Usuário atualizada com sucesso", user: user});        }
        throw new Error();
    } catch(err){
        return res.status(500).json({message:"Usuário não existe. Verifique o id e tente novamente..."});
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;

    try{
        const deleted = await User.destroy({where: {id: id}});

        if(deleted){
            return res.status(200).json("Usuário deletado!")
        } throw new Error();

    } catch(err){
        return res.status(500).json({message:"Usuário não existe!"})
    }

};

module.exports = {
    index,
    show,
    destroy,
    create, 
    update,

}


