const {response} = require("express");
const Store = require ("../../models/Store");

const create = async (req, res) => {

    try{
        const store = await Store.create(req.body);
        return res.status(201).json({message: "Loja cadastrada com sucesso!", store: store});


    } catch(err){
        res.statusgits(500).json({message: "Erro ao cadastrar a loja. Verifique e tente novamente..."});
    }
};


const show = async(req, res) => {
    const {id} = req.params;

    try{
        const store = await Store.findByPk(id);
        // if para que store caia em um erro caso não exista e apareça uma mensagem para o usuário
        if(store == null){
            store = 'undefined'; 
        }
        return res.status(200).json({store});

    } catch(err){

        return res.status(500).json({message: "Loja não existe. Verifique o id e tente novamente..."});

    }


};


const index = async(req, res) => {

    try{
        const store = await Store.findAll();
        return res.status(200).json({store});


    } catch(err){
        return res.status(500).json({message: "Erro ao encontrar as lojas. Tente novamente..."});
    }

};


const update = async (req, res) => {
    const {id} = req.params;

    try{

        const {updated} = await Store.update(req.body, {where: {id: id}});

        if(!updated) {
            const store = await Store.findByPk(id);

             // if para que store caia em um erro caso não exista e apareça uma mensagem para o usuário
             if(store == null){
                store = 'undefined'; 
            }
            return res.status(200).json({message: "Loja atualizada com sucesso", store:store});
        }
        throw new Error();
    } catch(err){
        return res.status(500).json({message:"A loja não existe. Verique o id e tente novamente..."});
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;

    try{
        const deleted = await Store.destroy({where: {id: id}});

        if(deleted){
            return res.status(200).json({message:"Loja deletada!"})
        } throw new Error();

    } catch(err){
        return res.status(500).json({message:"Loja não existe!"})
    }

};

module.exports = {
    index,
    show,
    destroy,
    create, 
    update,

}