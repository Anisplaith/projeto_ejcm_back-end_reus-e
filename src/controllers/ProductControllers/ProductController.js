const {response} = require("express");
const Product = require ("../../models/Product");
const Store = require("../../models/Store");
const User = require("../../models/User");

const create = async(req, res) => {

    try{
        const products = await Product.create(req.body);
        return res.status(201).json({message: "Produto cadastrado com sucesso!", products: products});


    } catch(err){
        res.status(500).json({message: "Erro ao cadastrar o produto. Verifique e tente novamente..."});
    }
}

const show = async(req, res) => {
    const {id} = req.params;

    try{
        const product = await Product.findByPk(id);

        // if para que product caia em um erro caso não exista e apareça uma mensagem para o usuário
        if(product == null){
            product = 'undefined'; 
        }
        return res.status(200).json({product});

    } catch(err){

        return res.status(500).json({message: "Produto não existe. Verifique o id e tente novamente..."});

    }


};


const index = async(req, res) => {

    try{
        const products = await Product.findAll();
        return res.status(200).json({products});


    } catch(err){
        return res.status(500).json({message: "Erro ao encontrar os produtos. Tente novamente..."});
    }

};


const update = async (req, res) => {
    const {id} = req.params;

    try{

        const {updated} = await Product.update(req.body, {where: {id: id}});

        if(!updated) {
            const product = await Product.findByPk(id);
            
             // if para que product caia em um erro caso não exista e apareça uma mensagem para o usuário
             if(product == null){
                product = 'undefined'; 
            }
            return res.status(200).json({message: "Produto atualizado com sucesso", product:product});
        }
        throw new Error();
    } catch(err){
        return res.status(500).json("O produto não existe, Verifique o id e tente novamente...");
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;

    try{
        const deleted = await Product.destroy({where: {id: id}});

        if(deleted){
            return res.status(200).json("Produto deletado!")
        } throw new Error();

    } catch(err){
        return res.status(500).json("Produto não existe!")
    }

};

const addRelation = async(req, res) =>{

    const {id} = req.params;
    
    try{
        const product = await Product.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        const store = await Store.findByPk(req.body.StoreId);
        await product.setUser(user);
        await product.setStore(store);
        return res.status(200).json(product);
    } catch(err){
        return res.status(500).json({message: "A relação não pôde ser concluída. Verifique o id e tente novamente..."});
    }
}

const deleteRelationUser = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await product.setUser(null);
        return res.status(200).json(product);
    } catch(err){
        return res.status(500).json({message: "A relação não pôde ser removida. Verifique o id e tente novamente..."})
    }
}

const deleteRelationStore = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await product.setStore(null);
        return res.status(200).json(product);
    } catch(err){
        return res.status(500).json({message: "A relação não pôde ser removida. Verifique o id e tente novamente..."})
    }
}

const deleteRelations = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await product.setUser(null);
        await product.setStore(null);
        return res.status(200).json(product);
    } catch(err){
        return res.status(500).json({message: "As relações não puderam ser removidas. Verifique o id e tente novamente..."})
    }
}








module.exports = {
    index,
    show,
    destroy,
    create, 
    update,
    addRelation,
    deleteRelationUser,
    deleteRelationStore,
    deleteRelations

}
