const { Sequelize } = require("sequelize");
const DataType = require("sequelize");
const sequelize = require("../config/sequelize");
const User = require("../models/User");
const Store = require("../models/Store")

const Product = sequelize.define('Product', {
    // id gerado automaticamente
    name: {

        type: DataType.STRING,
        allowNull: false

    },
    info_tec: {

        type: DataType.TEXT

    },
    price: {

        type: DataType.DOUBLE,
        allowNull: false

    },
    photo: {

        type: DataType.TEXT,
        allowNull: false

    },
    amount: {

        type: DataType.NUMBER, //string?
        allowNull: false

    }

},{
    timestamps: false
});

/*Product.associate = function(models) {
    
    Product.belongsTo(models.User);
    Product.belongsTo(models.Store);

};*/

Product.belongsTo(User);
Product.belongsTo(Store);


module.exports = Product;