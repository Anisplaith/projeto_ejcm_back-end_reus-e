const DataType = require("sequelize");
const sequelize = require("../config/sequelize");
const Product = require("./Product");

const Store = sequelize.define('Store', {

    // id gerado automaticamente
    name: {

        type: DataType.STRING,
        allowNull: false

    },
    phone: {

        type: DataType.STRING, 
        allowNull: false

    },
    email: {

        type: DataType.STRING,
        allowNull: false

    }

},{
    timestamps: false
});

Store.associations = function(models){
    Store.hasMany(models.Product);

};






module.exports = Store;