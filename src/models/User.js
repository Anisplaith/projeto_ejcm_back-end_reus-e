const DataType = require("sequelize");
const sequelize = require("../config/sequelize");


const User = sequelize.define('User', {
    // id gerado automaticamente
    name: {

        type: DataType.STRING,
        allowNull: false

    },
    cpf: {

        type: DataType.STRING,
        allowNull: false

    },
    email: {
        type: DataType.STRING,
        allowNull: false


    },
    password: {

        type: DataType.STRING,
        allowNull: false

    },
    birthday: {

        type: DataType.DATEONLY,
        allowNull: false

    },
    phone: {

        type: DataType.STRING

    }


},{
    timestamps: false
});

User.associations = function(models){
    User.hasMany(models.Product);
};
    

module.exports = User;