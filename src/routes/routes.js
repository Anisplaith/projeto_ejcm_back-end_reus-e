const {Router} = require("express");
const ProductController = require("../controllers/ProductControllers/ProductController");
const StoreController = require("../controllers/StoreControllers/StoreController");
const UserController = require("../controllers/UserControllers/UserController");
const router = Router();

/* ----Routers de Product---- */

router.get("/products", ProductController.index);
router.get("/products/:id", ProductController.show);
router.post("/products", ProductController.create);
router.put("/products/:id", ProductController.update);
router.delete("/products/:id", ProductController.destroy);
router.put("/relationadd/:id", ProductController.addRelation);
// deletar apenas o User ou Store ou ambos
router.delete("/relationDelUser/:id", ProductController.deleteRelationUser);
router.delete("/relationDelStore/:id", ProductController.deleteRelationStore);
router.delete("/relationsDel/:id", ProductController.deleteRelations);



/* ----Routers de User---- */

router.get("/users", UserController.index);
router.get("/users/:id", UserController.show);
router.post("/users", UserController.create);
router.put("/users/:id", UserController.update);
router.delete("/users/:id", UserController.destroy);

/* ----Routers de Store---- */

router.get("/stores", StoreController.index);
router.get("/stores/:id", StoreController.show);
router.post("/stores", StoreController.create);
router.put("/stores/:id", StoreController.update);
router.delete("/stores/:id", StoreController.destroy);


/* ----Routers de Relationship---- */



module.exports = router;